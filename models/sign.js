var mongoose = require('mongoose');

var SignSchema = new mongoose.Schema({
    rut: {
        type: String,
        unique: true,
        required: true,
        trim: true
    }
    // phone: {
    //     type: String,
    //     unique: true,
    //     required: true,
    //     trim: true
    // }
});

var Sign = mongoose.model('Sign', SignSchema);
module.exports = Sign;