var mongoose = require('mongoose');

var CensusSchema = new mongoose.Schema({
    filename: {
        type: String,
        unique: false,
        required: true,
        trim: true
    },
    data: {
        type: Array,
        required: true
    },
    header: {
        type: String,
        required: true
    },
    census_type: {
        type: String,
        required:true
    }
}, { timestamps: { createdAt: 'createdAt' }
});

var Census = mongoose.model('Census', CensusSchema);
module.exports = Census;