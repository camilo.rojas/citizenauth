var mongoose = require('mongoose');

var CensusOffSchema = new mongoose.Schema({
    filename: {
        type: String,
        unique: false,
        required: true,
        trim: true
    },
    data: {
        type: Array
        // required: true
    },
    header: {
        type: String,
        required: true
    },
    census_type: {
        type: String,
        required:true
    }
}, { timestamps: { createdAt: 'createdAt' }
});

var CensusOff = mongoose.model('CensusOff', CensusOffSchema);
module.exports = CensusOff;