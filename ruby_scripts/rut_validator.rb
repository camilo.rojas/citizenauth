require 'csv'
require 'watir'
require 'headless'
require 'optparse'

options = {}
OptionParser.new do |opts|

  opts.banner = 'Usage: rut_validator.rb [options]'

  opts.on('-fFILE', '--file=FILE', 'Validates ruts from file') do |f|
     options[:file] = f
  end

  opts.on('-rRUT', '--rut=RUT', 'Validates one rut') do |r|
    options[:rut] = r
  end

  opts.on('-cCODE', '--c=CODE', 'Validates from code') do |c|
    options[:code] = c
  end
end.parse!

class RutValidator

  @rows = Array.new
  @browser = nil

  def read_csv(file_path)
    @rows = CSV.read(file_path)
  end

  def sanitize_rows
    1.upto(@rows.length-1) do |num|
      puts @rows[num][0]
      @rows[num][0] = @rows[num][0].to_s.strip.gsub(/-|\.|\*| /, "")
      @rows[num][1] = @rows[num][1].to_s.strip
    end
  end

  def validate_rut(rut, code)
    raw_rut = rut.to_s.strip.gsub(/-|\.|\*| /, "")
    dig = rut[rut.length-1..rut.length-1]
    rut = raw_rut[0..raw_rut.length-2] + '-' +  dig

    if digito_verificador(raw_rut[0..raw_rut.length-2]).to_s  != dig.to_s
        res = 'rut invalid'
        return res
    else
        p 'rut valid'
    end


    code = code.to_s.strip
    headless = Headless.new
    headless.start
    @browser = Watir::Browser.new :firefox


    begin
        @browser.goto 'https://portal.sidiv.registrocivil.cl/usuarios-portal/pages/DocumentRequestStatus.xhtml'
        res = validate_one_rut(rut, code)
    rescue StandardError => e
        p 'Error ' + e.to_s
        res = 'error'
    end
    @browser.close
    headless.destroy
    return res

  end

  def digito_verificador(rut)
    [*0..9,'k'][rut.to_s.reverse.chars.inject([0,0]){|(i,a),n|[i+1,a-n.to_i*(i%6+2)]}[1]%11]
  end

  def validate_rows
    @browser = Watir::Browser.new

    vigentes =0
    novigentes =0
    malos=0

    conf_dig = ['k', 'K', '0']


    CSV.open("resultados.csv", "w") do |csv|
      1.upto(@rows.length-1) do |num|




        raw_rut = @rows[num][0]
        rut = raw_rut
        dig = rut[rut.length-1..rut.length-1]
        res = ""
        val = @rows[num][1]

        if conf_dig.include? dig
          it = conf_dig
        else
          it = [dig]
        end

        # if dig=='k' || dig=='K' || dig=='0'
        #   dig='k'
        # end

        if raw_rut == nil or raw_rut== ""
          next
        end

        it.each do |newdig|

          dig = newdig
          rut = raw_rut[0..raw_rut.length-2] + '-' +  dig

          if val[0] == 'a'
            val[0] ='A'
          end

          if val[1] == 'o' || val[1] == 'O'
            val[1] ='0'
          end


          estado = @rows[num][2]

          #if(estado != "ESTADO Vigente")
          if(estado ==nil || estado == "" || estado == "ESTADO")
            @browser.goto 'https://portal.sidiv.registrocivil.cl/usuarios-portal/pages/DocumentRequestStatus.xhtml'
            res = validate_one_rut(rut, val)
          else
            res = estado
          end

          if res== "ESTADO Vigente"
            break
          end

        end


        if res== 'ESTADO Vigente'
          vigentes = vigentes+1

        elsif res == 'ESTADO No Vigente'
          novigentes = novigentes+1

        elsif res == 'ESTADO'
          malos = malos +1
        end



        puts rut + ' ' + val + ' ' + res + ' ' + num.to_s
          csv << [rut, val, res]
      end
    end

    totales = vigentes + novigentes + malos
    puts 'TOTALES PROCESADOS: ' + totales.to_s
    puts 'VIGENTES: ' + vigentes.to_s + ' ' + (vigentes*1.0/totales).to_s
    puts 'NO VIGENTES: ' + novigentes.to_s + ' ' + (novigentes*1.0/totales).to_s
    puts 'MALOS: ' + malos.to_s + ' ' + (malos*1.0/totales).to_s
  end

  def validate_one_rut(rut, validatecode)
    p "validating rut"

    begin
      rut_number = rut[0..rut.length-2]
      rut_ver = rut[rut.length-1..rut.length-1]
      @browser.text_field(:id => 'form:run').set rut_number+rut_ver
      @browser.select_list(:id, "form:selectDocType").select("CEDULA")
      @browser.text_field(:name => 'form:docNumber').set validatecode
      links = @browser.links
      links[1].click
      validation = @browser.table(:id, "tableResult").text
      if  validation.include? "Vigente"
        return validation
      else
        @browser.goto 'https://portal.sidiv.registrocivil.cl/usuarios-portal/pages/DocumentRequestStatus.xhtml'
        @browser.text_field(:id => 'form:run').set rut_number+rut_ver
        @browser.select_list(:id, "form:selectDocType").select("CEDULA_EXT")
        @browser.text_field(:name => 'form:docNumber').set validatecode
        links = @browser.links
        links[1].click
        validation = @browser.table(:id, "tableResult").text
      end
      return validation
      
    rescue StandardError => e
      return 'Error ' + e.to_s
    end

  end

end

if options[:file]
  # Example for validation from csv
  FILE = options[:file]
  r = RutValidator.new
  r.read_csv FILE
  r.sanitize_rows
  r.validate_rows
elsif options[:rut] and options[:code]
  # Example for one rut validation
  rut = options[:rut]
  code = options[:code]
  r = RutValidator.new
  puts r.validate_rut(rut, code)
end


