#Donwload Chromewebbrowser
sudo apt-get install -y xvfb
sudo apt-get install -y firefox
sudo apt-get install -y wget

if [ ! -d "bin" ]; then
  echo "Creating bin folder"
  mkdir bin
fi
cd bin
if [ ! -f "geckodriver" ]; then
  echo "Downloading geckodriver"
  wget https://github.com/mozilla/geckodriver/releases/download/v0.24.0/geckodriver-v0.24.0-linux64.tar.gz
  tar -zxvf geckodriver-v0.24.0-linux64.tar.gz
fi

# Set ADD_PATH variable to add geckodriver to .bashrc
if [ ! -z "$ADD_PATH" ]; then
    add_to_path=`pwd`;
    if (! grep -q "PATH.*$add_to_path" ~/.bashrc); then
    echo "Setting up PATH: Adding $add_to_path to PATH";
    echo "PATH=\$PATH:$add_to_path" >> ~/.bashrc;
    source ~/.bashrc
    fi
fi
cd -

