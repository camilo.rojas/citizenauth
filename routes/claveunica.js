var express = require('express');
let router = express.Router();
const util = require('util');
const exec = util.promisify(require('child_process').exec);
var csv = require("fast-csv");
const bcrypt = require('bcrypt');
const https = require('https');
const querystring = require('querystring');
const axios = require('axios')

const client_keys = {
  sandbox: {id: 'e725a55bf6a64c8591f02616da14179f', secret: '6a1c69a44e2e48cea43167fcf4240bc7' },
  cu: {id: 'c3561f4c2cba4b288a3add2c9ce55f1a', secret: '9f001947891f43bdaf531b7372db48a6' }
};

router.get('/cu/form', function (req, res, next) {
  // return res.sendFile(path.join(__dirname + '/views/index.html'));
  return res.render('testcu', {} );
});


router.post('/cu/testcu', function (req, res, next) {
  if (req.body.type) {
    if(req.body.type === 'cusandbox') {
      get_params = querystring.stringify({
        client_id: client_keys.sandbox.id,
        redirect_uri: 'https://autentificacion.xyz/testclaveunica',
        response_type: 'code',
        scope: 'openid run name',
        state: req.body.state
      });
    } else if (req.body.type === 'cu') {
      get_params = querystring.stringify({
        client_id: client_keys.cu.id,
        redirect_uri: 'https://autentificacion.xyz/claveunica',
        response_type: 'code',
        scope: 'openid run name',
        state: req.body.state
      });
    } else {
      res.send('type error');
    }
    res.redirect('https://accounts.claveunica.gob.cl/openid/authorize/?' + get_params);
  } else {
    res.send('error with params');
  }
});


router.get('/testclaveunica', function (req, res, next) {

  //TODO: check if state is the same than before
  console.log('state:', req.query.state);
  console.log('testing sandbox callback claveunica');

  let post_headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'charset' :'UTF-8'
  };

  let post_data =querystring.stringify( {
    client_id: client_keys.sandbox.id,
    client_secret: client_keys.sandbox.secret,
    redirect_uri: 'https://autentificacion.xyz/testclaveunica',
    grant_type: 'authorization_code',
    code: req.query.code,
    state: req.query.state
  });

  console.log('post_data: ', post_data);

  axios.post('https://accounts.claveunica.gob.cl/openid/token/', post_data, {headers: post_headers} )
    .then((p_res) => {
      console.log(`statusCode: ${p_res.statusCode}`);
      // console.log('p_res: ', p_res);
      // Obtain info about user
      let p_info_header = {
         'Authorization': 'Bearer ' + p_res.data.access_token
      };
      axios.post('https://www.claveunica.gob.cl/openid/userinfo/', '', {headers: p_info_header} )
        .then((info_res) => {
          console.log('info status code: ', info_res.statusCode);
          console.log('info data: ', info_res.data);
          return res.json(info_res.data)
        })
        .catch((error) => {
          console.error('error: ',  error);
        })
    })
    .catch((error) => {
      console.error('error info: ',  error);
    });
});


router.get('/claveunica', function (req, res, next) {

  //TODO: check if state is the same than before
  console.log('state:', req.query.state);
  console.log('testing sandbox callback claveunica');

  let post_headers = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'charset' :'UTF-8'
  };

  let post_data =querystring.stringify( {
    client_id: client_keys.cu.id,
    client_secret: client_keys.cu.secret,
    redirect_uri: 'https://autentificacion.xyz/claveunica',
    grant_type: 'authorization_code',
    code: req.query.code,
    state: req.query.state
  });

  console.log('post_data: ', post_data);

  axios.post('https://accounts.claveunica.gob.cl/openid/token/', post_data, {headers: post_headers} )
    .then((p_res) => {
      console.log(`statusCode: ${p_res.statusCode}`);
      // console.log('p_res: ', p_res);
      // Obtain info about user
      let p_info_header = {
        'Authorization': 'Bearer ' + p_res.data.access_token
      };
      axios.post('https://www.claveunica.gob.cl/openid/userinfo/', '', {headers: p_info_header} )
        .then((info_res) => {
          console.log('info status code: ', info_res.statusCode);
          console.log('info data: ', info_res.data);
          return res.json(info_res.data)
        })
        .catch((error) => {
          console.error('error: ',  error);
        })
    })
    .catch((error) => {
      console.error('error info: ',  error);
    });
});


module.exports = router;
