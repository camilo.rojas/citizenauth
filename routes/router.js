var express = require('express');
var router = express.Router();
var User = require('../models/users');
var Census = require('../models/census');
var CensusOff = require('../models/census_off');
var Sign = require('../models/sign');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
var csv = require("fast-csv");
var Readable = require('stream').Readable;
const bcrypt = require('bcrypt');
var fs  = require('fs');

var falloString = 'Algo fallo.';
var habilitadoString = 'Estas habilitado/a para votar en ';
var noHabilitadoString = 'No estas habilitado/a para votar.';
var yaVotasteString = 'Ya contamos con tu voto, muchas gracias.';
const inner_salt = "$2b$05$eqdoXt4/h3AtlncSTk6.N.";

const tok_key = 'MmFlMTRmNDhlNmY5M2JkMmNiMDRlZDFj';

// GET route for reading data
router.get('/', function (req, res, next) {
    // return res.sendFile(path.join(__dirname + '/views/index.html'));
    return res.render('index', {} );
});


//POST new user creation
router.post('/', function (req, res, next) {
    if (req.body.username &&
        req.body.password &&
        req.body.key === tok_key) {

        req.body.username = clean_rut(req.body.username);
        // if(req.body.email) {
        let userData = {
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
            reg_state: req.body.reg_state
        };

        User.findOne({username: req.body.username}, function(err, user) {
            if (user) {
                //update
                // console.log(user);
                // console.log(err);
                User.update(user, userData, function (error, user) {
                    if (error) {
                        return next(error);
                    } else {
                        return res.json({
                            status: 'updated'
                        });
                    }
                });
            } else {
                //creating
                User.create(userData, function (error, user) {
                    if (error) {
                        return next(error);
                    } else {
                        return res.json({
                            status: 'created'
                        });
                    }
                });
            }
        });
    } else {
        console.log('Something is wrong :(');
        return res.status(400).send('Wrong key');
    }
});

router.get('/verify', function(req, res, next) {
    if(req.query.rut && req.query.number)
    {
        req.query.rut =  clean_rut(req.query.rut);
        req.query.number = clean_code(req.query.number);

        User.findOne({username: req.query.rut}, async function(err, user) {
            if(err){
                return res.json({
                    status: 'fail',
                    message: falloString
                })
            }

            let cen = Census;
            if(req.query.padron && req.query.padron === 'servel'){
                cen = CensusOff;
            }

            var census = await obtain_census(req.query.rut, cen);

            let esta_padron = false;
            if(census.nacional !== '' ) {
                esta_padron = true;
            }

            if (user)
            {
                User.authenticate(req.query.rut, req.query.number, async function (error, user) {
                    let verification;
                    let test_mode = req.query.testtoken === tok_key;
                    console.log('test mode: ' + test_mode);
                    if (error || !user || test_mode) {
                        verification =  await verify_national_register(req, esta_padron);
                    }
                    else {
                        verification = {
                            status: 'success',
                            rut: req.query.rut,
                            padron: esta_padron
                        }
                    }
                    return res.json(verification);
                });
            } else {
                let verification = await verify_national_register(req, esta_padron);
                return res.json(verification);
            }
        });
    }
    else {
        return res.json({
            status: 'fail', message: 'Al salio mal, intentalo de nuevo mas tarde'
        })
    }
});


// Vote in election
router.get('/vote', function(req, res, next) {
    console.log('dirname:', );
    if(req.query.rut && req.query.number)
    {
        req.query.rut =  clean_rut(req.query.rut);
        req.query.number = clean_code(req.query.number);
        // req.query.phone = clean_phone(req.query.phone);

        let sign = Sign.findOne({rut: req.query.rut}, function(err, sign) {
            if(sign){
                return res.json({
                    status: 'fail',
                    message: yaVotasteString
                })
            } else {

                // let phoneQuery = Sign.findOne({phone: req.query.phone});
                // phoneQuery.then(function (sign) {
                    if (sign) {
                        return res.json({
                            status: 'fail',
                            message: "Ya se voto con ese telefono"
                        })
                    } else {
                        User.findOne({username: req.query.rut}, async function (err, user) {
                            if (err) {
                                return res.json({
                                    status: 'fail',
                                    message: falloString
                                })
                            }

                            var census = await obtain_census(req.query.rut, Census);

                            let esta_padron = false;
                            if (census.nacional !== '') {
                                esta_padron = true;
                            }

                            // var nacional_validation = false;
                            // if( (census.nacional !== '' && req.query.nacional !== 'false')
                            //     || (census.nacional === '' && req.query.nacional === 'false') ){
                            //     nacional_validation = true;
                            // }

                            // let regional_validation = false;
                            // if( (census.regional === '' && req.query.regional === 'false')
                            //     || (census.regional === req.query.regional) ) {
                            //     regional_validation = true;
                            // }

                            // var regional_validation2 = false;
                            // if( (census.regional2 === '' && req.query.regional2 === 'false')
                            //     || (census.regional2 === req.query.regional2) ) {
                            //     regional_validation2 = true;
                            // }

                            // var basal_validation = false;
                            // if( (census.basal === '' && req.query.basal === 'false')
                            //     || (census.basal === req.query.basal) ) {
                            //     basal_validation = true;
                            // }

                            if (!esta_padron) {
                                return res.json({
                                    status: 'fail',
                                    message: 'El rut no esta incluido en el padron :('
                                });
                            } else if (user) {
                                User.authenticate(req.query.rut, req.query.number, async function (error, user) {
                                    // if not athenticated
                                    let verification = {status: 'fail', message: 'Al salio mal, intentalo de nuevo mas tarde'};

                                    if (error || !user) {
                                        verification = await verify_national_register(req, esta_padron);
                                    } else {
                                        console.log('User authenticated by system');
                                        verification = {
                                            status: 'success',
                                            rut: req.query.rut,
                                            padron: esta_padron
                                        }
                                    }

                                    if (verification.status === 'success') {
                                        let signed = await sign_book(req);
                                        if (signed.success) {
                                            verification.hash = signed.hash;
                                            return res.json(verification);
                                        } else {
                                            return res.json({
                                                status: 'fail',
                                                message: 'Al salio mal, intentalo de nuevo mas tarde'
                                            });
                                        }
                                    } else {
                                        return res.json(verification);
                                    }
                                });
                            } else {
                                let verification = await verify_national_register(req, esta_padron);
                                if (verification.status === 'success') {
                                    let signed = await sign_book(req);
                                    if (signed.success) {
                                        verification.hash = signed.hash;
                                        return res.json(verification);
                                    } else {
                                        return res.json({
                                            status: 'fail',
                                            message: 'Al salio mal, intentalo de nuevo mas tarde'
                                        });
                                    }
                                } else {
                                    return res.json(verification);
                                }
                            }
                        });
                    }
                // });


            }
        });
    }
    else {
        return res.json({
            status: 'fail'
        })
    }
});

async function verify_national_register(req, padron){
    return new Promise( async function(resolve) {
        console.log('executing ruby script');
        let root_path = process.cwd();
        const {stdout, stderr} = await exec('ruby ' + root_path + '/ruby_scripts/rut_validator.rb -r ' + req.query.rut + ' -c ' + req.query.number).catch(
            function(error) {
                console.log("error catch" + error);
                resolve({
                    status: 'fail',
                    message: 'Error de conexion, porfavor intente mas tarde'
                });
            });

        // let stderr = false;
        // let stdout = "ESTADO Vigente\n";
        if (stderr) {
            console.error(`error: ${stderr}`);
            console.log('Error de conexion');
            resolve({
                status: 'fail',
                message: 'Error de conexion, porfavor intente mas tarde'
            });
        }

        // let stdout = "ESTADO Vigente\n";
        console.log('stdout: ' + stdout);

        if (stdout.includes("ESTADO Vigente\n")) {
            let userData = {
                email: req.query.email,
                username: req.query.rut,
                password: req.query.number,
                reg_state: 'Vigente'
            };

            User.findOne({username: req.query.rut}, function (err, user) {
                if (user) {
                    User.update(user, userData, function (error, user) {
                        resolve({
                            status: 'success',
                            rut: req.query.rut,
                            padron: padron
                        });
                    });
                } else {
                    User.create(userData, function (error, user) {
                        resolve({
                            status: 'success',
                            rut: req.query.rut,
                            padron: padron
                        });
                    });
                }
            });
        } else if (stdout.includes("ESTADO No Vigente\n")) {
            let userData = {
                email: req.query.email,
                username: req.query.rut,
                password: req.query.number,
                reg_state: 'No Vigente'
            };

            User.findOne({username: req.query.rut}, function (err, user) {
                if (user) {
                    User.update(user, userData, function (error, user) {
                        resolve({
                            status: 'success',
                            rut: req.query.rut,
                            padron: padron
                        });
                    });
                } else {
                    User.create(userData, function (error, user) {
                        resolve({
                            status: 'success',
                            rut: req.query.rut,
                            padron: padron
                        });
                    });
                }
            });
        } else {
            resolve({
                status: 'fail',
                message: 'El numero de documento no corresponde'
            });
        }
    });
}

// POSt to upload file
router.post('/padron', function(req, res) {
    console.log(req.body.token);
    if (!req.body.token || req.body.token !=='MmFlMTRmNDhlNmY5M2JkMmNiMDRlZDFj')
        return res.status(400).send('Wrong token');
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let csvFile = req.files.sampleFile;
    let arrayData = [];
    let arrayHeaders = [];
    let arrayCensusType = [];
    console.log(csvFile);

    var s = new Readable();
    s.push(csvFile.data);   // the string you want
    s.push(null);
    s.pipe(csv.parse({ headers: true }))
    .on("data", function(row){
        // console.log(data);

        if(arrayHeaders.length === 0) {
            arrayHeaders = Object.keys(row);
            console.log(arrayHeaders.length);
        }

        if(arrayCensusType.length === 0) {
            arrayCensusType = Object.values(row);
            console.log(arrayCensusType.length);
        } else{
            if(arrayData.length === 0) {
                Object.values(row).forEach(function(val, index, array) {
                    let cleanRut = clean_rut(val);
                    if(cleanRut !== "-undefined") {
                        arrayData.push([cleanRut]);
                    } else {
                        arrayData.push([]);
                    }
                });
            }
            else {
                arrayHeaders.forEach(function(head, index, array) {
                    let cleanRut = clean_rut(row[head]);
                    if(cleanRut !== "-undefined") {
                        arrayData[index].push(cleanRut);
                    }
                });
            }
        }

        // console.log(arrayData);
    })
    .on("end", function(){
        console.log("done");
        Census.remove({}, function(err) {
            console.log('collection removed');
            arrayHeaders.forEach(function(head, index, array) {
                let censusData = {
                    filename: csvFile.name,
                    data: arrayData[index],
                    header: head.replace(/\n/g, ''),
                    census_type: arrayCensusType[index]
                };

                if(censusData.header !== '' &&
                    censusData.census_type !=='' &&
                    censusData.data.length !== 0 ) {
                    console.log(censusData);

                    Census.create(censusData, function (error, census) {
                        if(error) {
                            console.log("hola error");
                            res.send(error);
                        }
                        console.log('created');
                    });

                }
            });
        });
        res.send('File uploaded!');
    });
});


// Get if user is inscribed
router.get('/rut', async function(req, res, next) {
    if(req.query.rut)
    {
        req.query.rut =  clean_rut(req.query.rut);
        let dic = await obtain_census(req.query.rut, Census);
        let signQuery = Sign.findOne({rut: req.query.rut});
        signQuery.then(function(sign) {
            dic.sign = sign? true : false;
            return res.json(dic);
        });
    }
    else {
        // var err = new Error('No rut or number.');
        // err.status = 400;
        // return next(err);
        return res.json({
            message: falloString
        });
    }
});

router.get('/rutoff', async function(req, res, next) {
    if(req.query.rut)
    {
        req.query.rut =  clean_rut(req.query.rut);
        let dic = await obtain_census(req.query.rut, CensusOff);
        return res.json(dic);

    }
    else {
        // var err = new Error('No rut or number.');
        // err.status = 400;
        // return next(err);
        return res.json({
            message: falloString
        });
    }
});

async function obtain_reg_census(rut, cen) {
    return new Promise(function(resolve) {
        cen.findOne({}, {}, {sort: {'createdAt': -1}}, function (err, census) {
            if (err) {
                resolve({
                    message: falloString
                });
            }

            if (census) {
                console.log(census.filename);
                // let cenArr = cen.find({filename: census.filename});
                let messRegional = '';

                // let votIndex = 0;
                cen.find()
                .where("filename", census.filename)
                .where("census_type", 'Opt-cp')
                .exec(function (err, censusArray) {
                    censusArray.forEach(function (cens, index) {
                        if (cens.data.includes(rut)) {
                            messRegional += cens.header + ', ';
                        }
                    });
                    resolve({
                        regional: messRegional.slice(0, -2),
                        rut: rut
                    });
                });

            }
            else {
                resolve({
                    message: falloString
                });
            }
        });
    });
}

async function obtain_census(rut, cen) {

    return new Promise(function(resolve) {
        cen.findOne({}, {}, {sort: {'createdAt': -1}}, function (err, census) {
        if (err) {
            resolve({
                message: falloString
            });
        }

        if (census) {
            console.log(census.filename);
            let cenArr = cen.find({filename: census.filename});
            let messNacional = '';
            let messRegional = '';
            let messBasales = '';
            let messRegional2 = '';

            let votIndex = 0;
            cen.find()
                .where("filename", census.filename)
                .where("census_type", 'Opt-cp')
                .exec(function (err, censusArray) {
                    censusArray.forEach(function (cens, index) {
                        if (cens.data.includes(rut)) {
                            messNacional += cens.header + ', ';
                        }
                    });
                    cen.find()
                        .where("filename", census.filename)
                        .where("census_type", 'Opt-reg')
                        .exec(function (err, censusArray) {
                            censusArray.forEach(function (cens, index) {
                                if (cens.data.includes(rut)) {
                                    messRegional += cens.header + ', ';
                                }
                            });
                            cen.find()
                                .where("filename", census.filename)
                                .where("census_type", 'Opt-basales')
                                .exec(function (err, censusArray) {
                                    censusArray.forEach(function (cens, index) {
                                        if (cens.data.includes(rut)) {
                                            messBasales += cens.header + ', ';
                                        }
                                    });
                                    cen.find()
                                        .where("filename", census.filename)
                                        .where("census_type", 'Opt-reg2')
                                        .exec(function (err, censusArray) {
                                            censusArray.forEach(function (cens, index) {
                                                if (cens.data.includes(rut)) {
                                                    messRegional2 += cens.header + ', ';
                                                }
                                            });

                                            resolve({
                                                    nacional: messNacional.slice(0, -2),
                                                    regional: messRegional.slice(0, -2),
                                                    basal: messBasales.slice(0, -2),
                                                    regional2: messRegional2.slice(0, -2),
                                                    rut: rut
                                            });

                                        });
                                });
                        });
                });
        }
        else {
            resolve({
                message: falloString
            });
        }
    });
});
}

// Get to padron form
router.get('/padron', function (req, res, next) {
    // return res.sendFile(path.join(__dirname + '/views/index.html'));
    return res.render('padron', {} );
});


function clean_rut(raw_rut){
    let rut  = raw_rut;

    let re = /[^0-9|^K|^k]/g;
    rut = rut.replace(re, '');
    rut =  rut.replace(/K/g, 'k');
    if(rut[0] == '0') {
        rut = rut.slice(1);
    }

    let code =  rut[rut.length-1];
    rut =  rut.substring(0, rut.length-1) + '-' + code;

    return rut;
}

function clean_code(raw_code){
    let re = /\./g;
    let code = raw_code.replace(re, '');
    re = /a/;
    return code.replace(re, 'A');
}

function clean_phone(raw_phone){
    let re = /[^0-9]/g;
    let phone = raw_phone.replace(re, '');
    return phone;
}

async function sign_book(req){
    return new Promise(function(resolve) {
        let inner_hash = bcrypt.hashSync(req.query.rut + req.query.number, inner_salt).substring(29, 61);
        let signData = {
            rut: req.query.rut
            // phone: req.query.phone
        };

        Sign.create(signData, function (error) {
            if (error) {
                console.log("Error al crear Firma");
                resolve({
                    success: false
                });
            }else {

                resolve({success: true, hash: inner_hash});
            }
        });
    });
}

module.exports = router;
