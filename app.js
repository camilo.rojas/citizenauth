var express = require('express');
var app = express();
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var fileUpload = require('express-fileupload');
var cors = require('cors');

// var session = require('express-session');
// var MongoStore = require('connect-mongo')(session);

//connect to MongoDB
let DB_USER = (process.env.CIT_DB_USER === undefined) ? 'kaminari' :process.env.CIT_DB_USER;
let DB_PASSWORD = (process.env.CIT_DB_PASS === undefined) ? 'V0t0Digital': process.env.CIT_DB_PASS;
let DB_HOST = (process.env.CIT_DB_HOST === undefined) ? 'localhost': process.env.CIT_DB_HOST;
let mongo_url = 'mongodb://' + DB_USER + ':' + DB_PASSWORD + '@' + DB_HOST +'/CitizenAuth'
mongoose.connect(mongo_url, { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true });
var db = mongoose.connection;

//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


//use sessions for tracking logins
// app.use(session({
//     secret: 'work hard',
//     resave: true,
//     saveUninitialized: false,
//     store: new MongoStore({
//         mongooseConnection: db
//     })
// }));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(fileUpload());
app.use(cors());


// include routes
var routes = require('./routes/router');
let cuvars = require('./routes/claveunica');
app.use('/', routes);
app.use('/', cuvars);

// app.use('/', index);
// app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
