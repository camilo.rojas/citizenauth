FROM ubuntu:18.04

RUN apt-get update -qq && apt-get install -y build-essential sudo apt-utils curl git
RUN apt-get install -y zlib1g-dev libssl1.0-dev libreadline-dev libyaml-dev libxml2-dev libxslt-dev
RUN apt-get install -y python-pip
RUN apt-get install -y net-tools iputils-ping nano
RUN apt-get install -y xvfb
RUN apt-get install -y firefox
RUN apt-get install -y wget
RUN apt-get clean

ENV NVM_DIR /root/.nvm
ENV NODE_VERSION 10.16.2

# Install nvm with node and npm
RUN mkdir -p $NVM_DIR
RUN curl https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash \
    && . $NVM_DIR/nvm.sh \
    && nvm --version \
    && nvm install $NODE_VERSION 

ENV NODE_PATH $NVM_DIR/versions/node/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN node --version
RUN npm --version

# Install rbenv and ruby-build
RUN git clone https://github.com/sstephenson/rbenv.git /root/.rbenv
RUN git clone https://github.com/sstephenson/ruby-build.git /root/.rbenv/plugins/ruby-build
RUN /root/.rbenv/plugins/ruby-build/install.sh
ENV PATH /root/.rbenv/bin:$PATH
RUN echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh # or /etc/profile
RUN echo 'eval "$(rbenv init -)"' >> .bashrc
ENV PATH /root/.rbenv/bin/:$PATH
RUN rbenv install 2.5.5
ENV PATH /root/.rbenv/shims:/root/.rbenv/versions/2.5.5/bin:$PATH
# Test rbenv installed
RUN echo $PATH
RUN ruby --version
RUN gem --version

WORKDIR /citizen
ADD . /citizen

WORKDIR ruby_scripts
RUN gem update --system
RUN gem install bundler
RUN bundle install
RUN sh install_webdriver.sh
ENV PATH /citizen/ruby_scripts/bin:$PATH
RUN geckodriver --version

WORKDIR /citizen
RUN npm install

EXPOSE 3000

ENV CIT_DB_USER kaminari
ENV CIT_DB_PASS V0t0Digital
ENV CIT_DB_HOST 172.17.0.1

CMD npm start


